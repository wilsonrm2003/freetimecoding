import java.util.*;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Hangman {
    public static void main(String[] args) throws IOException {
	// TODO Auto-generated method stub
	Scanner keyboard = new Scanner(System.in);
	String choice = "";
	int guessesLeft = 15;
	ArrayList<String> dictwords = (ArrayList<String>) Files.readAllLines(Paths.get("/Users/921687/Projects/freetimecoding/Java/dictionary.txt"), Charset.defaultCharset());
	ArrayList<String> words = new ArrayList<String>();
	String[] temp;
	for(String line : dictwords) {
	    temp = line.split("  ");
	    if(temp[0].length() > 0) {
		words.add(temp[0]);
	    }
	}
	String wordToGuess = "";
	String encryptedWord = "";
	do {
	    System.out.println("Welcome to HANGMAN");
	    System.out.println("You have 15 guesses to get it right!");
	    System.out.println("(pls dont kill him)");
	    System.out.println("Do you want to play Multiplayer or Singleplayer?");
	    choice = keyboard.nextLine();
	    if(choice.equals("Multiplayer")) {
	        System.out.println("Type in a word for the other player to guess.");
		wordToGuess = keyboard.nextLine().toLowerCase();
		words.add(wordToGuess);
	    }
	    else if(choice.equals("Singleplayer")) {
		Random random = new Random();
		int x = random.nextInt(words.size());
		wordToGuess = words.get(x).toLowerCase();
	    }
	    else {
		System.out.println("Do you want to play Multiplayer or Singleplayer?");
		choice = keyboard.nextLine();
	    }
	    StringBuilder sb = new StringBuilder();
	    for(int i=0 ; i < wordToGuess.length(); i++) {
	        sb.append('*');
	    }
	    encryptedWord = sb. toString();
	    boolean gameover = false; 
	    boolean guess = false;
	    while(!gameover) {
		System.out.println(encryptedWord);
	        System.out.println("Guess a letter.");
		char letterGuess = keyboard.nextLine().toLowerCase().charAt(0);
		for(int i = 0; i < encryptedWord.length(); i++) {
		    char letter = wordToGuess.charAt(i);
		    if(letterGuess == letter) {
		        sb = new StringBuilder(encryptedWord);
			sb.setCharAt(i, letterGuess);
			encryptedWord = sb.toString();	 
		        System.out.println("You Are Correct!");
		        guess = true;
		        System.out.println("You have " + guessesLeft + " left.");
		    } 	
		}
		if(!guess == true){
		    System.out.println("You Are Wrong.");
		    guess = false;
		    guessesLeft -= 1;
		    System.out.println("You have " + guessesLeft + " left.");
		}
		guess = false;
		if(guessesLeft == 0) {
		    gameover = true;	
		    System.out.println("Gameover! The word was " + wordToGuess);
		    guessesLeft = 15;
		    System.out.println("Do you want to play again?");
		    choice = keyboard.nextLine();
		}
		if(wordToGuess.equals(encryptedWord)) {
		    gameover = true;	
		    System.out.println("Gameover! The word was " + wordToGuess);
		    guessesLeft = 15;
		    System.out.println("Do you want to play again?");
		    choice = keyboard.nextLine();
		}
		if(choice.equals("no")){
		    System.out.println("Goodbye! ;D");
		}
	    }
	}
        while(!choice.equals("no"));
	    PrintStream file = new PrintStream("dictionary.txt");
	    for(int i=0 ; i<words.size(); i++) {
	        file.println(words.get(i));
	    }
	    keyboard.close();
	    file.close();
	}
		
}
